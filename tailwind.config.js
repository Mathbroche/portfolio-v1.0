module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    
    extend: {
      fontSize:{
        '4xl': '2.25rem',
      },
      textDecorationThickness: {
        thin: '1px',
      },
      fontFamily:{
        'test': ['Inter'],
        'soustitre': ['Roboto Slab'],
        },
      colors: {
        'e': '#FFE1C2',
        'Almond': '#FFEBD6',
        'Lightergrey': '#494649',
        'Backgrey': '#151415',
        'Darkgrey': '#1f1e1f',
        'Midgrey': '#2F2D2F',
        'Lightgrey': '#3e3c3e',
        'BackgroundLight':'#192339',
        'BackgroundDarker':'#0C111C'
      },
      spacing: {
        '128': '32rem',
      }
    },
  },
  plugins: [],
  
}
