import React from "react";
import Typical from 'react-typical'
import content from "../content";

export default function PersonnalExp() { 

    return (
    <section class="text-gray-400 bg-BackgroundLight body-font overflow-hidden min-h-screen">
    <h1 className="flex justify-center mx-auto mt-48 text-4xl text-Almond">Mon Expérience Professionnelle</h1>
    <div class="container px-5 py-24 flex justify-center mx-auto min-w-">
        <div class="-my-8 divide-y-2 divide-gray-800">
        <div class="py-8 flex flex-wrap md:flex-nowrap">
            <div class="md:w-64 md:mb-0 mb-6 flex-shrink-0 flex flex-col">
            <span class="font-semibold title-font text-white">Seconde Année BTS</span>
            <span class="mt-1 text-gray-500 text-sm">Janvier 2022 - Juin 2022 </span>
            </div>
            <div class="md:flex-grow">
            <h2 class="text-2xl font-medium text-white title-font mb-2">Développeur PHP (Symfony)</h2>
            <p class="leading-relaxed">- Développement d'une application web permettant la gestion complète d'un planning.</p>
            <p class="leading-relaxed">- Mise en production de l'application web.</p>
            <p class="leading-relaxed">- Mise en oeuvre d'une méthode de travail agile.</p>
            <p class="leading-relaxed">- Ecriture d'une documentation complète.</p>
            </div>
        </div>
        <div class="py-8 flex border-t-2 border-gray-800 flex-wrap md:flex-nowrap">
            <div class="md:w-64 md:mb-0 mb-6 flex-shrink-0 flex flex-col">
            <span class="font-semibold title-font text-white">Première Année BTS</span>
            <span class="mt-1 text-gray-500 text-sm">Mai 2021 - Juin 2021</span>
            </div>
            <div class="md:flex-grow">
            <h2 class="text-2xl font-medium text-white title-font mb-2">Développeur WEB</h2>
            <p class="leading-relaxed">- Apprentissage du CMS "Wordpress".</p>
            <p class="leading-relaxed">- Maintenance de sites internet.</p>
            <p class="leading-relaxed">- Référencement de sites internet.</p>
            </div>
        </div>
        <div class="py-8 flex border-t-2 border-gray-800 flex-wrap md:flex-nowrap">
            <div class="md:w-64 md:mb-0 mb-6 flex-shrink-0 flex flex-col">
            <span class="font-semibold title-font text-white"></span>
            <span class="mt-1 text-gray-500 text-sm"></span>
            </div>
            <div class="md:flex-grow">
            <h2 class="text-2xl font-medium text-white title-font mb-2"></h2>
            <p class="leading-relaxed text-BackgroundLight select-none">Glossier echo park pug, church-key sartorial biodiesel vexillologist pop-up snackwave ramps cornhole. Marfa 3 wolf moon party messenger bag selfies, poke vaporware kombucha lumbersexual pork belly polaroid hoodie portland craft beer.</p>
            </div>
        </div>
        </div>
    </div>
    </section>
        
    )
}