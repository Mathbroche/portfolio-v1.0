import React from "react";
import { Link } from "react-router-dom";
import content from "../content";

export default function About() {

<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300&display=swap" rel="stylesheet"></link>

    return (
<section class="min-h-screen">

<div class="w-full h-screen flex items-center justify-center bg-gray-900">
    <div class="relative w-128 h-auto bg-Darkgrey rounded-md pt-24 pb-8 px-4 shadow-md hover:shadow-lg transition flex flex-col items-center">
        <div class="absolute rounded-full bg-e w-32 h-32 p-2 z-10 -top-8 shadow-lg hover:shadow-xl transition">
            <div class="rounded-full bg-black w-full h-full overflow-auto">
                <img src=""></img>
            </div>
        </div>
        <label class="font-bold text-gray-100 text-lg my-2">
            Mathis Broche
        </label>
        <p class="text-center text-gray-200 mt-2 leading-relaxed">
            Actuellement étudiant en BTS SIO <a class="font-bold"> ( Service Informatique aux Organisations )</a> souhaitant poursuivre ses études en licence professionnelle.
        </p>
        <ul class="flex flex-row gap-2 mt-4">
                <a href="https://gitlab.com/Mathbroche" target="_blank">
                    <img src="https://img.icons8.com/ios-glyphs/60/ffffff/github.png"/>
                </a>

                <a href="#" target="_blank">
                <img src="https://img.icons8.com/ios-glyphs/60/ffffff/linkedin.png"/>
                </a>

                <button onClick={() => {navigator.clipboard.writeText("mathbroche@gmail.com"); window.alert('L\'adresse mail à été copiée dans le presse-papier');  }}>
                    <img src="https://img.icons8.com/material-rounded/60/ffffff/mail.png"/>
                </button>
                
                <Link to="/CV">
                <button>
                <img href="/test" src="https://img.icons8.com/material-rounded/60/ffffff/resume.png"/>
                </button>
                </Link>
                
        </ul>
    </div>
</div>

</section>

    )
}