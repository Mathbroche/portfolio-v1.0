import React from "react";
import Typical from 'react-typical'
import content from "../content";

export default function Header() {
<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300&display=swap" rel="stylesheet"></link>
    

    return (
        <div className="flex flex-col">
        <div className="min-h-screen  bg-BackgroundDarker flex">
            <div className=" flex-col flex justify-center items-center mx-auto">
                <h1 className="Titre text-8xl font-test text-Almond" >Mathis Broche </h1>
                <div className="ml-96">
                    <Typical
                        className="text-4xl ml-60 mt-0 pt-0 text-Almond font-soustitre underline decoration-thin underline-offset-2"
                        steps={['Étudiant BTS SIO', 1000, 'Développeur Back-end', 500]}
                        loop={Infinity}
                        wrapper="p"
                    />
                </div>
            </div>
            <div className="icon-scroll flex item-center"></div>
        </div>
        
        </div>
        
    )
}