import React from "react";
import Typical from 'react-typical'
import content from "../content";
import { Link } from "react-router-dom";

export default function Header() {
<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300&display=swap" rel="stylesheet"></link>
    

    return (
    <section class=" body-font min-h-screen bg-gray-900">
    
    <div class="container px-5 py-24 mx-auto">
        <h1 className="flex justify-center mx-auto mb-20 text-4xl text-Almond mt-24">Quelques-uns de mes projets</h1>
        <div class="flex flex-wrap -m-4">
        <div class="p-4 lg:w-1/3">
            <div class="h-full bg-BackgroundLight bg-opacity-75 px-8 pt-16 pb-24 rounded-lg overflow-hidden text-center relative">
            <h2 class="tracking-widest title-font font-medium text-white mb-1">Projet d'école</h2>
            <h1 class="title-font sm:text-2xl text-xl font-medium text-white mb-3">MATETE</h1>
            <p class="leading-relaxed mb-3 text-white">Application web et mobile visant à mettre en relation particulier et producteur de fruits et légumes.</p>
            <a class="text-white inline-flex items-center" href="https://gitlab.com/RomainChardon/matete" target="_blank">Voir plus
                <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path d="M5 12h14"></path>
                <path d="M12 5l7 7-7 7"></path>
                </svg>
            </a>
            </div>
        </div>
        <div class="p-4 lg:w-1/3">
            <div class="h-full bg-BackgroundLight bg-opacity-75 px-8 pt-16 pb-24 rounded-lg overflow-hidden text-center relative">
            <h2 class="tracking-widest text-xs title-font font-medium text-white mb-1">Projet d'école</h2>
            <h1 class="title-font sm:text-2xl text-xl font-medium text-white mb-3">Veille Technologique</h1>
            <p class="leading-relaxed mb-3 text-white">Exercice de veille technologique.Mon thème : Blockchain et je traiterai de la popularisation qu'ont subit les cryptomonnaies c'est 2 dernières années</p>
            <Link to="/Veille">
                <a class="text-white inline-flex items-center">{content.nav.links.text} Voir plus
                    <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M5 12h14"></path>
                    <path d="M12 5l7 7-7 7"></path>
                    </svg>
                </a>
            </Link>
            
            
            </div>
        </div>
        <div class="p-4 lg:w-1/3">
            <div class="h-full bg-BackgroundLight bg-opacity-75 px-8 pt-16 pb-24 rounded-lg overflow-hidden text-center relative">
            <h2 class="tracking-widest text-xs title-font font-medium text-white mb-1">Mission de Stage</h2>
            <h1 class="title-font sm:text-2xl text-xl font-medium text-white mb-3">Application web Planning</h1>
            <p class="leading-relaxed mb-3 text-white">Application WEB permettant la gestion complete d'un planning de garde périscolaire. Application développée sous PHP (Symfony)</p>
            <a class="text-white inline-flex items-center" href="https://gitlab.com/Mathbroche/calendrier-periscolaire" target="_blank">Voir plus
                <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path d="M5 12h14"></path>
                <path d="M12 5l7 7-7 7"></path>
                </svg>
            </a>
            </div>
        </div>
        </div>
    </div>
    </section>

        
    )
}