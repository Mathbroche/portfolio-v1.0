import react from "react"
import content from '../content'

export default function Studies() {
    const [openTab, setOpenTab] = react.useState(1);
    return (
        <div className="min-h-screen flex flex-col items-center justify-center font-robotoSlab bg-gray-900"  id="studies">
            <div>
                <h1 className="text-3xl md:text-5xl pt-5 md:pb-11 text-center font-test text-Almond">Mes études</h1>
            </div>
            <div className="w-11/12 md:max-w-3xl text-xl text-center">
                <div className="relative flex flex-col min-w-0 break-words w-full mb-6 rounded">
                    <div className="px-4 py-5 flex-auto">
                        <div className="bg-Backgrey shadow overflow-hidden sm:rounded-t-lg">
                            {content.etudes.etude.map((studyMap, index) => {
                                return (
                                    <div className={openTab === index + 1 ? "block" : "hidden"} id={'link' + (index + 1)}>
                                        <div className="px-4 py-5 sm:px-6">
                                            <h3 className="text-lg leading-6 font-medium text-Almond">
                                                {studyMap.diploma}
                                            </h3>
                                            <p className="mt-1 max-w-2xl text-sm text-Almond">
                                                {studyMap.fullDiplomaName}
                                            </p>
                                        </div>
                                        <div className="border-t border-b border-gray-800">
                                            <dl>
                                                <div className="bg-Darkgrey px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                    <dt className="text-sm font-medium text-white">
                                                        Years :
                                                    </dt>
                                                    <dd className="mt-1 text-sm text-white sm:mt-0 sm:col-span-2">
                                                        {studyMap.years}
                                                    </dd>
                                                </div>
                                                <div className="bg-Midgrey px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                    <dt className="text-sm font-medium text-white">
                                                        At :
                                                    </dt>
                                                    <dd className="mt-1 text-sm text-white sm:mt-0 sm:col-span-2">
                                                        {studyMap.school}
                                                    </dd>
                                                </div>
                                                <div className="bg-Darkgrey px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                    <dt className="text-sm font-medium text-white">
                                                        School Address :
                                                    </dt>
                                                    <dd className="mt-1 text-sm text-white sm:mt-0 sm:col-span-2">
                                                        {studyMap.address}
                                                    </dd>
                                                </div>
                                                <div className="bg-Midgrey px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                    <dt className="text-sm font-medium text-white">
                                                        Website :
                                                    </dt>
                                                    <dd className="mt-1 text-sm text-white sm:mt-0 sm:col-span-2 underline">
                                                        <a target="_blank" rel="noopener noreferrer"  href={studyMap.webisteData}>
                                                            {studyMap.website}
                                                        </a>
                                                    </dd>
                                                </div>
                                            </dl>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                        <div className="bg-Backgrey shadow overflow-hidden sm:rounded-b-lg">
                            <div className="px-4 py-5 sm:px-6">
                                <ul className="flex mb-0 list-none flex-wrap pt-3 pb-4 flex-row" role="tablist">
                                    {content.etudes.etude.map((studyMap2, index) => {
                                        return (
                                            <li className="-mb-px mr-2 pt-2 md:pt-0 last:mr-0 flex-auto text-center">
                                                <a className="text-xs font-bold uppercase px-5 py-3 shadow-lg rounded block leading-normal"
                                                    style={(openTab === (index + 1)
                                                        ? { color: 'black', background: '#FFCC99' }
                                                        : { color: 'black', background: '#FFEBD6' }
                                                    )}
                                                    onClick={e => {
                                                        e.preventDefault();
                                                        setOpenTab(index + 1);
                                                    }}
                                                    data-toggle="tab" href={'#link' + (index + 1)} role="tablist">
                                                    {studyMap2.diploma}
                                                </a>
                                            </li>
                                        );
                                    })}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};