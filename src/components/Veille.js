import React from "react";


export default function Veille() {

<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300&display=swap" rel="stylesheet"></link>


    return (
        <div className="min-h-screen flex flex-col items-center pt-32 font-robotoSlab bg-gray-900"  id="studies">
            <div>
                <h1 className="text-3xl md:text-5xl text-Almond font-bold">Veille Technologique</h1>
            </div>
            <div>
                <p className="text-Almond pt-5 text-lg">Le thème de ma veille est <a className="font-bold">"La blockchain"</a>.Cependant dû au manque d'article traitant de la 
                technologie blockchain en elle même j'ai élargi mes recherches jusqu'aux usages fait avec celle-ci.</p>
                <p className="flex justify-center text-Almond pt-5 text-lg"> Je vais donc parler de la popularisation des crypto-monnaies</p>
            </div>

        <div class="flex flex-wrap justify-center pt-9">
        <div class="xl:w-1/4 md:w-1/2 p-4">
            <div class="bg-gray-100 p-6 rounded-lg" >
            <img src="https://i.ibb.co/4ty0svh/WW.png" alt="WW" border="0"></img>
            <a class="tracking-widest text-indigo-500 text-xs font-medium title-font" href="https://journalducoin.com/actualites/watches-world-montres-luxe-bitcoin-cryptomonnaies/" target="_blank">Article</a>
            <h2 class="text-lg text-gray-900 font-medium title-font mb-4">World Watches</h2>
            <p class="leading-relaxed text-base">Une marque de montre autorise l'achat et la revente de ses montres avec des bitcoins. </p>
            </div>
        </div>
        <div class="xl:w-1/4 md:w-1/2 p-4">
            <div class="bg-gray-100 p-6 rounded-lg">
            <img src="https://i.ibb.co/s2NwhqL/presskit-model-3-scaled-1.jpg" alt="presskit-model-3-scaled-1" border="0"></img>
            <a class="tracking-widest text-indigo-500 text-xs font-medium title-font" href="https://www.numerama.com/tech/846091-tesla-detient-desormais-2-milliards-de-dollars-en-bitcoins.html" target="_blank">Article Tesla /</a> <a class="tracking-widest text-indigo-500 text-xs font-medium title-font" href="https://journalducoin.com/nft/visa-confirme-interet-pour-crypto-nft-visa-pour-reussite/" target="_blank">Article Visa</a>
            <h2 class="text-lg text-gray-900 font-medium title-font mb-4">Tesla / Visa</h2>
            <p class="leading-relaxed text-base">Des entreprises font un dangereux paris sur les cryptomonnaies et les NFT.</p>
            </div>
        </div>
        <div class="xl:w-1/4 md:w-1/2 p-4">
            <div class="bg-gray-100 p-6 rounded-lg">
            <img src="https://i.ibb.co/mTvs5Jp/Crypto-World.webp" alt="Crypto-World" border="0"></img>
            <a class="tracking-widest text-indigo-500 text-xs font-medium title-font" href="https://www.numerama.com/tech/834453-ces-pays-pourraient-ils-aussi-adopter-le-bitcoin-en-monnaie-legale.html" target="_blank">Article</a>
            <h2 class="text-lg text-gray-900 font-medium title-font mb-4">Le Monde</h2>
            <p class="leading-relaxed text-base">De grands états considèrent les cryptomonnaies comme une monnaie courante.</p>
            </div>
        </div>
        </div>
        </div>
    )
}