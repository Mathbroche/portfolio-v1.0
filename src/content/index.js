export default {
    nav: {
        links: [
            { text: 'Veille', to: 'veille' },
        ],
    },
    header: {
        text: [''],
    },
    etudes: {
        etude: [
            {
                diploma: 'Baccalauréat',
                years: '2019-2020',
                school: 'Lycée Pablo Picasso',
                address: '120 Av. Général Gilles, 66000 Perpignan',
                website: 'https://pablo-picasso.mon-ent-occitanie.fr',
                fullDiplomaName: 'BAC STI2D , Système d\'Information et Numérique ',
            },
            {
                diploma: 'BTS',
                years: '2021-2022',
                school: ' Lycée Jean Lurçat',
                address: '25 Av. Albert Camus, 66000 Perpignan',
                website: 'https://lyc-lurcat-perpignan.ac-montpellier.fr',
                fullDiplomaName: 'BTS SIO , Solution Logiciel et Application Métier',
            },
        ]
    },
    professionalExp: {
        experiences: [
            {
                postes: 'Developpeur Symfony',
                dates: 'Janvier 2022 - Février 2022',
                workplace: 'Ecole les lucioles',
                address: '13 Rue des Écoles, 66320 Marquixanes',
                website: 'https://www.ecoleleslucioles.com',
                task1: 'Concevoir et créer une application web permettant de gérer un emploi du temps',
                task2: '',
                techUsed1: 'PHP 7.3 / MySQL / PostgreSQL',
            },
            {
                postes: 'Developpeur WEB',
                dates: 'Mai 2021 - Juin 2021',
                workplace: 'AGR',
                address: '6 rue Aristide Bergès Les Madrès, 66330 Cabestany',
                website: 'agr-informatique.com',
                task1: 'Maintenance des sites web de l\'entreprise ',
                task2: '',
                techUsed1: 'Wordpress',
            },
        ]
    },
    myProject: {
        projects: [
            {
                name: '"GIE" Project',
                desc: 'In-Developement project, 3 developer actively working on it (2 backends developer, 1 frontend developer). Project based on the Laravel framework.',
                link: 'https://site-gie.chrisblnc.fr/',
            },
            {
                name: 'Mail Server',
                desc: 'Creation of a mail server with a graphical interface (RainLoop), anti-spam and mail signature with SPF/DKIM(OpenDKIM)/DMARC. Server based on a Debian VPS.',
                link: '',
            },
            {
                name: 'Generation of a complete web architecture under docker',
                desc: 'First approach of Docker, and application containerization under Linux. Project consisting of a sandbox, part of production, as well as all continuous development tools.',
                link: '',
            },
            {
                name: '"Mesguen" Project (Thin Client)',
                desc: 'Project aiming to create a lightweight client based on a specification provides. This web application aims to manage the various tours of the company "Mesguen". (Project in no case related to the company "Mesguen" and any name affiliated to Mesguen).',
                link: 'https://github.com/Emile-G/PPE_Mesguen/',
            },
            {
                name: '"Mesguen" Project (Heavy Client)',
                desc: 'Iteration of the project "Mesguen" light in heavy client. Based on VB.Net code and an Oracle database. It is an application that aims to manage the various tours of the company "Mesguen". (Project in no case related to the company "Mesguen" and any name affiliated to Mesguen).',
                link: 'https://github.com/Emile-G/PPE_Mesguen_Lourd/',
            },
            {
                name: 'Staff tool for "Synesia"',
                desc: 'A tool to manage warnings from different players, project based on HTML/PHP linked to a MySQL database. (Basic CRUD locked by an authentication system).',
                link: '',
            },
        ]
    },
    links: {
        socialMedia: [
            {
                title: 'GitLab',
                link: 'https://gitlab.com/Mathbroche',
            },
            {
                title: 'LinkedIn',
                link: 'https://www.linkedin.com/in/emile-g/',
            },
        ]
    },
};