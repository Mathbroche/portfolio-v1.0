import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './components/Header';
import About from './components/About';
import Studies from './components/Studies';
import PersonnalExp from './components/PersonalExp';
import Project from './components/Project';
import CV from './components/CV';
import Skills from "./components/Skills";
import Veille from "./components/Veille";


function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/Veille">
            <Veille />
          </Route>
          <Route path="/CV">
            <CV />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );

  function Home() {
    return (
      <div>
        <Header />
        <About />
        <Skills />
        <Studies />
        <PersonnalExp />
        <Project />
      </div>
    );
  }
}

export default App;
